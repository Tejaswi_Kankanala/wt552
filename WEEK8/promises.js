let p = "CVR";
let promise = new Promise(function(resolve,reject){
    setTimeout(function(){
        resolve("This is resolved promise")
    },2000)
})
function onfullfill(result){
    console.log(result);
}
function onRejection(err){
    console.log(err)
}
promise.then(onfullfill).catch(onRejection);
console.dir(promise)
   
let p1 = new Promise((resolve,reject)=>{
    let x = 10 ;
    if(x<10){
        resolve(x);
    }else{
        reject("Value of x should be less than 10")
    }
})
console.log(p1);
p1.then(res=>console.log(res)).catch(err=>console.log(err))

function add(x){
    return x+100;
}
function sub(x){
    return x-50;
}
function mul(x){
    return x*100;
}

let p2 = new Promise(function(resolve,reject){
    resolve(10);
})
    console.log(p2.then(add)
                .then(sub)
                .then(mul)
                .then(res=>console.log(res)))
